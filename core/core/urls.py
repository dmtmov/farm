from django.contrib import admin
from django.urls import path, include

from animals.routers import router as animals_router
from farms.routers import router as farms_router
from reservation.routers import router as reservations_router
from core.swagger import urlpatterns as swagger_urls


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include(swagger_urls)),
    path("api/v1/animals/", include(animals_router.urls)),
    path("api/v1/farms/", include(farms_router.urls)),
    path("api/v1/reservations/", include(reservations_router.urls)),
]
