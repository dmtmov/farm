from django.contrib import admin
from farms.models import Farm


class FarmAdmin(admin.ModelAdmin):
    model = Farm
    list_display = ('title', 'get_animals')

    @admin.display(description='Animals')
    def get_animals(self, obj):
        return list(obj.animals.all())


admin.site.register(Farm, FarmAdmin)
