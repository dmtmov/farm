from farms.views import FarmViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register('', FarmViewSet)
