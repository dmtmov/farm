from farms.models import Farm
from rest_framework import serializers
from animals.serializers import AnimalSerializer


class FarmSerializer(serializers.ModelSerializer):
    animals = AnimalSerializer(many=True, read_only=True)

    class Meta:
        model = Farm
        fields = ['id', 'title', 'animals']
