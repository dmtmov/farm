from rest_framework import viewsets
from farms.serializers import FarmSerializer
from farms.models import Farm


class FarmViewSet(viewsets.ModelViewSet):
    queryset = Farm.objects.all()
    serializer_class = FarmSerializer
