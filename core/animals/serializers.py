from animals.models import Animal
from rest_framework import serializers


class AnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Animal
        fields = '__all__'


class BuyAnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Animal
        fields = ['farm']
