from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from animals.serializers import AnimalSerializer, BuyAnimalSerializer
from animals.models import Animal


class AnimalViewSet(viewsets.ModelViewSet):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer

    @action(detail=True, methods=['post'])
    def buy(self, request, pk=None):
        instance = self.get_object()
        serializer = BuyAnimalSerializer(data=request.data)
        if serializer.is_valid():
            instance.farm = serializer.validated_data['farm']
            instance.save()
            return Response({'status': 'success'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
