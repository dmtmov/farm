from django.db import models


class Animal(models.Model):
    title = models.CharField(max_length=50)
    farm = models.ForeignKey("farms.Farm", related_name="animals", on_delete=models.RESTRICT)

    def __str__(self):
        return self.title
