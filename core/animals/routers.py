from django.urls import include, re_path
from animals.views import AnimalViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register('', AnimalViewSet)
