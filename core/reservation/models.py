from django.db import models
from animals.models import Animal
from farms.models import Farm


PENDING = 0
APPROVED = 1
REJECTED = 2
STATUS = (
    (PENDING, 'Pending'),
    (APPROVED, 'Approved'),
    (REJECTED, 'Rejected'),
)


class ReservationRequest(models.Model):
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    farm = models.ForeignKey(Farm, on_delete=models.CASCADE)
    status = models.IntegerField(choices=STATUS, default=PENDING)

    def __str__(self):
        return f"{self.farm} - {self.animal} - {self.get_status_display()}"
