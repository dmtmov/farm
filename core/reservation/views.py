from rest_framework import viewsets
from reservation.serializers import ReservationSerializer
from reservation.models import ReservationRequest


class ReservationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ReservationRequest.objects.all()
    serializer_class = ReservationSerializer
