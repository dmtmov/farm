from reservation.models import ReservationRequest
from rest_framework import serializers
from animals.serializers import AnimalSerializer
from farms.serializers import FarmSerializer


class ReservationSerializer(serializers.ModelSerializer):
    animals = AnimalSerializer(many=True, read_only=True)
    farm = FarmSerializer(read_only=True)

    class Meta:
        model = ReservationRequest
        fields = '__all__'
