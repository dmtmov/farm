# Farms & Animals

The application describes basic CRUD API over Farms and Animals and ability to buy Animal from one Farm to another one.
Additionally there is OpenAPI swagger page.

## Local setup
Clone the repo and go into it's folder. You have `poetry` to be preinstalled.  
Install dependencies, run migrations and local server:
```shell
cd core
poetry install
poetry run ./managepy migrate
poetry run ./managepy runserver
```

The API will be accessible by th link: http://127.0.0.1:8000/api/v1/docs

## Steps to improve  

As an awesome service it should have a bunch of options that are not covered in this repo:
- separate configs for environments: dev/stage/test/demo/prod;
- extensive documentation for local development, schemas, helpful links, etc. 
- CI/CD pipelines. This part should include builders, linters, testing steps; 
- Describing testing part, there have to be added unit tests and functional ones;
- As part of delivery pipelines, the proper container registry must be specified.
So we might deploy different versions on our environments;
- To scale our service we may use Load Balancer for the web traffic;
- Database scaling. In this part, we may add a support of read/write replicas,
add table sharding, use separate searching engine like ElasticSearch;
- Add support of various Payment systems, implement Transactions/Orders/Refunds;


## Roadmap
 - [x] Add OpenAPI with SwaggerUI
 - [ ] Finalize animal reservation
 - [ ] Add Roles and Permissions

